package br.com.gustavo.trabalhotodolist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import br.com.gustavo.trabalhotodolist.bd.AppDatabase
import br.com.gustavo.trabalhotodolist.bd.dao.TarefaDao
import br.com.gustavo.trabalhotodolist.bd.ui.TarefaAdapter
import br.com.gustavo.trabalhotodolist.bd.ui.TarefaListListener
import br.com.gustavo.trabalhotodolist.entidades.Tarefa
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_tarefa.*

class MainActivity : AppCompatActivity(), TarefaListListener {
    override fun tarefaAtualizada(tarefa: Tarefa) {
        tarefaDao.atualizar(tarefa)
    }

    override fun tarefaDeletada(tarefa: Tarefa) {
        tarefaDao.apagar(tarefa)
    }
    override fun tarefaCriada(tarefa: Tarefa) {
        tarefaDao.inserir(tarefa).toInt()

    }

    lateinit var db: AppDatabase
    lateinit var tarefaDao: TarefaDao
    lateinit var adapter: TarefaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "tarefas"
        )
            .allowMainThreadQueries()
            .addMigrations()
            .build()
        tarefaDao = db.tarefaDao()

        btAdicionar.setOnClickListener {
            var titulo = ""
            var descricao = ""
            var boolean = false

            var tarefa = Tarefa(titulo, descricao, boolean)

            adapter.adicionarTarefa(tarefa)
            listTarefa.scrollToPosition(0)

        }

//        btSalvar.setOnClickListener {
//            val titulo = txtTitulo.text.toString()
//            val descricao = txtDescricao.text.toString()
//            val tarefa = tarefaDao.buscaTarefa(tarefaDao.buscaUltima())
//
//            tarefa.descricao = descricao
//            tarefa.titulo = titulo
//        }

        atualizaLista()

        configurarRecycler()
    }

    fun atualizaLista() {
        val tarefas = tarefaDao.buscaTodas()
        adapter = TarefaAdapter(tarefas.toMutableList(), this)
        listTarefa.adapter = adapter
    }



    fun configurarRecycler() {
        listTarefa.layoutManager = LinearLayoutManager(
            this, RecyclerView.VERTICAL, false)
    }
}
