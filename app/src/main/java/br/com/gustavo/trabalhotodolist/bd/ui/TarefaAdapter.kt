package br.com.gustavo.trabalhotodolist.bd.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.gustavo.trabalhotodolist.R
import br.com.gustavo.trabalhotodolist.entidades.Tarefa
import kotlinx.android.synthetic.main.item_tarefa.view.*
import kotlinx.android.synthetic.main.item_tarefa_criada.view.*

class TarefaAdapter(private var tarefas: MutableList<Tarefa>,
                    private var listener: TarefaListListener):
    RecyclerView.Adapter<TarefaAdapter.TarefaViewHolder>() {
    var tarefaEdicao: Tarefa? = null

    fun adicionarTarefa(tarefa: Tarefa) {
        tarefaEdicao = tarefa
        tarefas.add(0, tarefa)
        notifyItemInserted(0)
    }
//    fun adicionarTarefaCriada(tarefa: Tarefa) {
//        tarefasCriadas.add(0, tarefa)
//
//    }
    override fun getItemViewType(position: Int): Int {
        val tarefa = tarefas[position]

        if(tarefa != tarefaEdicao){
            return R.layout.item_tarefa_criada
        }else{
            return R.layout.item_tarefa
        }

    }

//    fun notifyItemChanged (position: Int, payload: Tarefa){
//        val view = LayoutInflater
//            .from(parent.context)
//            .inflate(R.layout.item_tarefa_criada, parent, false)
//        return TarefaViewHolder(view)
//    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TarefaViewHolder {

        val view = LayoutInflater
            .from(parent.context)
            .inflate(viewType, parent, false)
        return TarefaViewHolder(view)
    }

    override fun getItemCount() = tarefas.size

    override fun onBindViewHolder(holder: TarefaViewHolder, position: Int) {
        val tarefa = tarefas[position]
        holder.preencherView(tarefa)
    }

    inner class TarefaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun preencherView(tarefa: Tarefa) {
            val posicao = tarefas.indexOf(tarefa)

            //itemView.setOnLongClickListener{
//                tarefa.status = true;
//                notifyItemChanged(posicao)
//                listener.tarefaAtualizada(tarefa)
            //}

            if(tarefa != tarefaEdicao){
                if(tarefa.status){
                itemView.txtStatus.setText(R.string.concluido)
                    itemView.share.visibility = View.VISIBLE
            } else{
                itemView.txtStatus.setText(R.string.naofeito)
            }
                itemView.txtTarefaCriada.text = tarefa.titulo
                itemView.setOnClickListener {
                    tarefaEdicao = tarefa
                    notifyItemChanged(posicao)
                }
                itemView.setOnLongClickListener{
                    with(this@TarefaAdapter) {
                        tarefa.status = !tarefa.status
                        notifyItemChanged(posicao)
                        listener.tarefaAtualizada(tarefa)
                    }
                    true
                }
            }else {
                itemView.txtTitulo.setText(tarefa.titulo)
                itemView.txtDescricao.setText(tarefa.descricao)
                itemView.btExcluir.setOnClickListener {
                    with(this@TarefaAdapter) {

                        tarefas.removeAt(posicao)
                        notifyItemRemoved(posicao)
                        listener.tarefaDeletada(tarefa)
                    }
                }

                itemView.btSalvar.setOnClickListener {
                    with(this@TarefaAdapter) {
                        tarefa.titulo = itemView.txtTitulo.text.toString()
                        tarefa.descricao = itemView.txtDescricao.text.toString()
                        tarefaEdicao = null

                        listener.tarefaAtualizada(tarefa)
                        
                        notifyItemChanged(posicao)
                    }
                }
            }

        }
    }
}
