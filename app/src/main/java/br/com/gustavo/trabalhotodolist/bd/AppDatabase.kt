package br.com.gustavo.trabalhotodolist.bd

import br.com.gustavo.trabalhotodolist.bd.dao.TarefaDao
import br.com.gustavo.trabalhotodolist.entidades.Tarefa
import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Tarefa::class), version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun tarefaDao(): TarefaDao
}
