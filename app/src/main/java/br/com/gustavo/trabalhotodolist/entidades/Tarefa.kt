package br.com.gustavo.trabalhotodolist.entidades

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tarefas")
data class Tarefa(
    var titulo: String,
    var descricao: String,
    var status: Boolean
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    val infoTarefa get() = "$titulo $descricao $status"

    override fun toString() = infoTarefa

}
