package br.com.gustavo.trabalhotodolist.bd.dao

import androidx.room.*
import br.com.gustavo.trabalhotodolist.entidades.Tarefa

@Dao
interface TarefaDao {
    @Query("SELECT * FROM tarefas")
    fun buscaTodas(): List<Tarefa>

    @Query("SELECT * FROM tarefas WHERE id = :id LIMIT 1")
    fun buscaTarefa(id: Int): Tarefa

    @Query("SELECT MAX(id) FROM tarefas")
    fun buscaUltima(): Int

    @Update
    fun atualizar(tarefa: Tarefa)

    @Insert
    fun inserir(tarefa: Tarefa): Long

    @Delete
    fun apagar(tarefa: Tarefa)
}
