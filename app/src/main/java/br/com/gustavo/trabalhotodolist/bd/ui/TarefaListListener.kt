package br.com.gustavo.trabalhotodolist.bd.ui

import br.com.gustavo.trabalhotodolist.entidades.Tarefa

interface TarefaListListener {
    fun tarefaDeletada(tarefa: Tarefa)
    fun tarefaCriada(tarefa: Tarefa)
    fun tarefaAtualizada(tarefa: Tarefa)
}
